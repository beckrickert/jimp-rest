var path = require('path');
require('dotenv').config({
    path: path.join('./', 'master.env')
});
var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var cronjobs = require('./cronjobs.js');
//EXPRESS
var app = express();
app.disable('x-powered-by');
//app.set('trust proxy', 1);

//ROUTES
var router_api = require('./routes/api.js');

//EXPRESS USES
app.use(morgan('combined'));
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());
app.use('/api/v1', router_api);
app.use('/', express.static('public'));

console.log("Initializing Express.");
app.listen(process.env.HOST_PORT);

//CRONJOBS
cronjobs.EveryMinuteJobs().start();