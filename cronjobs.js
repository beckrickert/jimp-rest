var CronJob = require('cron').CronJob;
var fse = require('fs-extra');
var path = require('path');

var cleanUpImages = (p) => {
    console.log("Cleaning up temporary image files.");
    p = (p == undefined) ? path.join(__dirname, "images") : p;
    fse.emptyDir(p, (err) => {
        if (err) { console.log(err); }
    });
}

module.exports = {
    EveryMinuteJobs: () => {
        return new CronJob('00 * * * * *', () => {
            cleanUpImages();
        }, null, true, process.env.TZ);
    }
}