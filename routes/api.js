var express = require('express');
var router = express.Router();
var path = require('path');
var slowDown = require("express-slow-down");
var jimp = require('jimp');
var isurl = require('is-url');
var randomstring = require('randomstring');

var speedLimiter = slowDown({
    windowMs: 1000 * 60,
    delayAfter: 5,
    delayMs: 100
});

router.get('/:format/*', speedLimiter, (req, res) => {
    //will detect encoded url
    var url = req.params[0];
    var format = req.params.format.toLowerCase();

    if (isurl(url)) {

        format = (format.toUpperCase() == "JPG") ? "JPEG" : format;

        switch (format.toUpperCase()) {
            case "PNG":
            case "JPEG":
            case "BMP":
                var file = `${randomstring.generate({
                    readable: true, capitalization: "lowercase"})}.${format}`;

                jimp.read(url)
                .then(image => {
                    return image
                    .quality(100)
                    .write(path.join(__dirname, '../', 'images', file), (err, f) => {
                        if (!err) {
                            console.log(`Created temp file ${file}`);
                            console.log(`Responding with converted image.`);
                            return res.sendFile(path.join(__dirname, '../', 'images', file));
                        }
                        else {
                            return res.status(500).json({
                                status: 500,
                                message: "Error while converting image.",
                                error: err
                            });
                        }
                    });
                }).catch(e => {
                    return res.status(400).json({
                        status: 400,
                        message: "Requested image source is not a JPEG, PNG, BMP, TIFF, or GIF."
                    });
                });
                break;
            default:
                return res.status(400).json({
                    status: 400,
                    message: "Requested format is not JPEG, PNG, or BMP."
                });
        }
    }
    else {
        return res.status(400).json({
            status: 400,
            message: "Requested image source is not a JPEG, PNG, BMP, TIFF, or GIF."
        });
    }

});

router.post('/*', (req, res) => {
    res.status(405).json("Method not allowed.");
});


module.exports = router;