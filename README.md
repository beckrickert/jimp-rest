# JIMPRest

Convert an image format with a URL on-the-fly. RESTful API with JIMP (JavaScript Image Manipulation Program) https://www.npmjs.com/package/jimp

## Installation

Download or Clone [this repository](https://gitlab.com/beckrickert/jimp-rest) to an install directory:

```bash
git clone https://gitlab.com/beckrickert/jimp-rest.git
```

## Usage

Run a Docker container by doing the following:

```bash
cd ./jimp-rest
cp .env-template master.env
docker build -t jimprest:latest .
docker run -d -p 8082:80 --name your-name-here jimprest:latest 
```

Now on your local machine, reverse proxy, or etc. navigate to:
```bash
your-domain-or-ip/api/v1/FORMAT/https://example.com
```
Where `FORMAT` is the export file format of the image. Can be either `PNG`, `JPG`, `JPEG`, or `BMP`.

Your source URL image can be a format of a `PNG`, `JPEG`, `BMP`, `TIFF`, `GIF`.

## License
[MIT](https://choosealicense.com/licenses/mit/)